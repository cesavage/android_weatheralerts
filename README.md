Weather Alerts
==============

Introduction
------------
Weather Alerts is an Android application that allows users to retrieve weather statements from the National Weather Service (NWS) by state in real-time.

Developed from scratch as part of related coursework at Ball State University, this project accepts user input via a GUI, queries a web-based data source, and returns query results to the user.

Technologies
------------
Java
Android Studio
BitBucket
RSS
XML

Requirements
------------
+ Android device running Android 6.0+
+ Device permission to save files.
+ Device permission to alter application installation restrictions.
+ An active internet connection.

Installation
------------
This application is not hosted in the Google Play store and must be 'sideloaded' to a device. To sideload the application, follow the steps below:

1. Locate the application's .apk from the source repository at app/release/WeatherAlerts.apk .
2. Copy WeatherAlerts.apk to the target device's local storage.
3. Using the device, navigate to the .apk file and open it.
4. If prompted, allow the installation of unknown apps on the target device.

Use
---
1. To start the application, tap the application's "umbrella" icon from the devices app library.
2. From the "Select a state to see alerts" drop-down menu, select the state for which you would like to see weather alerts.
3. From the list of displayed weather alerts, tap the title of an alert to see the alert's details.
4. From the "Weather Alert Details" view, tap, "View more information from the National Weather Service," to launch  your device's web browser and see more detailed information about the alert at the NWS's website.

Concepts Demonstrated
---------------------
+ Familiarity with the Java programming language.
+ Understanding and execution of Model-View-Controller (MVC) program architecture.
+ Understanding of object-oriented programming practices, including the creation and use of objects.
+ Delivery of "clean code", including the separation of functionality into classes, minimizing method sizes, choice of reasonable method and variable names, consistency in code formatting, appropriate use of comments, and the like.
+ Understanding of XML structure and parsing methods.
+ Request and receipt of data from internet-connected sources.
+ Development and debugging of software in Android Studio, an IntelliJ IDE.
+ Use of version control (Git) during software development.