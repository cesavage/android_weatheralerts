package bsu.edu.weatheralerts;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import bsu.edu.weatheralerts.WeatherAlert.WeatherAlert;

public class WeatherAlertStore {

    public static WeatherAlertStore sWeatherAlertStore;
    public List<WeatherAlert> mWeatherAlertList;

    public WeatherAlertStore(){
        mWeatherAlertList = new ArrayList<>();
    }

    public static WeatherAlertStore getWeatherAlertStore(){
        if (sWeatherAlertStore == null){
            sWeatherAlertStore = new WeatherAlertStore();
        }
        return sWeatherAlertStore;
    }

    public List getAllWeatherAlerts(){
        return mWeatherAlertList;
    }

    public WeatherAlert getWeatherAlertById(UUID weatherAlertId){
        for (WeatherAlert weatherAlert : mWeatherAlertList){
            if (weatherAlert.mWeatherAlertId.equals(weatherAlertId)){
                return weatherAlert;
            }
        }
        return null;
    }

    public void addAlert(WeatherAlert newWeatherAlert) {
        mWeatherAlertList.add(newWeatherAlert);
    }
}
