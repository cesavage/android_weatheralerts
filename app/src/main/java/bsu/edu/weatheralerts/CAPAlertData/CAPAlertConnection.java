package bsu.edu.weatheralerts.CAPAlertData;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import java.io.InputStream;
import java.net.URL;

import bsu.edu.weatheralerts.WeatherAlertStore;

public class CAPAlertConnection extends AsyncTask<Void, Void, WeatherAlertStore> {
    private String mStateAbbreviation;
    private Context mCurrentContext;

    public CAPAlertConnection(String stateAbbreviation, Context currentContext){
        super();
        mStateAbbreviation = stateAbbreviation;
        mCurrentContext = currentContext;
    }

    @Override
    protected WeatherAlertStore doInBackground(Void... voids) {
        try {
            URL nwsAlertsUrl = new URL("http://alerts.weather.gov/cap/ca.php?x=0");
            InputStream inputStream = nwsAlertsUrl.openConnection().getInputStream();
            CAPAlertXMLParser capAlertXmlParser = new CAPAlertXMLParser();

            return capAlertXmlParser.parseXML(mStateAbbreviation, mCurrentContext);
        }

        catch(Exception e){
            Log.e("ERROR", e.getMessage(), e);

            return null;
        }
    }
}
