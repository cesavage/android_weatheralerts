package bsu.edu.weatheralerts.CAPAlertData;

import android.content.Context;
import android.renderscript.ScriptGroup;
import android.util.Log;
import android.util.Xml;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

import bsu.edu.weatheralerts.WeatherAlert.WeatherAlert;
import bsu.edu.weatheralerts.WeatherAlertStore;

public class CAPAlertXMLParser {

    public CAPAlertXMLParser(){
    }

    public WeatherAlertStore parseXML(String stateName, Context currentContext) throws XmlPullParserException, IOException {
        String stateAbbreviation = getStateAbbreviation(stateName);
        URL capAlertsUrl = new URL("https://alerts.weather.gov/cap/" + stateAbbreviation + ".php?x=0");

        WeatherAlertStore weatherAlertStore = WeatherAlertStore.getWeatherAlertStore();
        weatherAlertStore.mWeatherAlertList.clear();

        String capEvent = "Default event";
        String summary = "Default summary";
        String capEffective = "Default effective";
        String capExpires = "Default expiration";
        String capUrgency = "Default urgency";
        String capSeverity = "Default severity";
        String capCertainty = "Default certainty";
        String link = "Default link";

        InputStream stream = capAlertsUrl.openConnection().getInputStream();

        XmlPullParserFactory xmlFactoryObject = XmlPullParserFactory.newInstance();
        XmlPullParser myparser = xmlFactoryObject.newPullParser();
        myparser.setInput(stream, "UTF-8");

        int event = myparser.getEventType();
        String name;
        while (event != XmlPullParser.END_DOCUMENT){

            if(event == XmlPullParser.START_TAG){
                name = myparser.getName();

                if (name.equals("link")){
                    link = myparser.getAttributeValue(0);
                }

                myparser.next();

                switch(name){
                    case "cap:event":
                        if(myparser.getEventType() == XmlPullParser.TEXT){
                            capEvent = myparser.getText();
                        }
                        break;

                        case "summary":
                        if(myparser.getEventType() == XmlPullParser.TEXT){
                            summary = myparser.getText();
                        }
                        break;

                        case "cap:effective":
                        if(myparser.getEventType() == XmlPullParser.TEXT){
                            capEffective = myparser.getText();
                        }
                        break;

                        case "cap:expires":
                        if(myparser.getEventType() == XmlPullParser.TEXT){
                            capExpires = myparser.getText();
                        }
                        break;

                        case "cap:urgency":
                        if(myparser.getEventType() == XmlPullParser.TEXT){
                            capUrgency = myparser.getText();
                        }
                        break;

                        case "cap:severity":
                        if(myparser.getEventType() == XmlPullParser.TEXT){
                            capSeverity = myparser.getText();
                        }
                        break;

                        case "cap:certainty":
                        if(myparser.getEventType() == XmlPullParser.TEXT){
                            capCertainty = myparser.getText();
                        }
                        break;

                        case "link":
                        if(myparser.getEventType() == XmlPullParser.TEXT){
                            link = myparser.getAttributeName(0);
                        }
                        break;
                }
            }

            if(event == XmlPullParser.END_TAG){
                name = myparser.getName();
                if (name.equals("entry")){
                    WeatherAlert newWeatherAlert = new WeatherAlert();

                    newWeatherAlert.capEvent = capEvent;
                    newWeatherAlert.summary = summary;
                    newWeatherAlert.capEffective = capEffective;
                    newWeatherAlert.capExpires = capExpires;
                    newWeatherAlert.capUrgency = capUrgency;
                    newWeatherAlert.capSeverity = capSeverity;
                    newWeatherAlert.capCertainty = capCertainty;
                    newWeatherAlert.link = link;

                    weatherAlertStore.addAlert(newWeatherAlert);
                }
                }

            event = myparser.nextToken();
        }

        return weatherAlertStore;
    }



    private String getStateAbbreviation(String stateName){
        String stateAbbreviation = "XX";

        switch (stateName){
            case "Alabama":
                stateAbbreviation = "al";
                break;
            case "Alaska":
                stateAbbreviation = "ak";
                break;

            case "Arizona":
                stateAbbreviation = "az";
                break;

            case "Arkansas":
                stateAbbreviation = "ar";
                break;

            case "California":
                stateAbbreviation = "ca";
                break;

            case "Colorado":
                stateAbbreviation = "co";
                break;

            case "Connecticut":
                stateAbbreviation = "ct";
                break;

            case "Delaware":
                stateAbbreviation = "de";
                break;

            case "Florida":
                stateAbbreviation = "fl";
                break;

            case "Georgia":
                stateAbbreviation = "ga";
                break;

            case "Hawaii":
                stateAbbreviation = "hi";
                break;

            case "Idaho":
                stateAbbreviation = "id";
                break;

            case "Illinois":
                stateAbbreviation = "il";
                break;

            case "Indiana":
                stateAbbreviation = "in";
                break;

            case "Iowa":
                stateAbbreviation = "ia";
                break;

            case "Kansas":
                stateAbbreviation = "ks";
                break;

            case "Kentucky":
                stateAbbreviation = "ky";
                break;

            case "Louisiana":
                stateAbbreviation = "la";
                break;

            case "Maine":
                stateAbbreviation = "me";
                break;

            case "Maryland":
                stateAbbreviation = "md";
                break;

            case "Massachusetts":
                stateAbbreviation = "ma";
                break;

            case "Michigan":
                stateAbbreviation = "mi";
                break;

            case "Minnesota":
                stateAbbreviation = "mn";
                break;

            case "Mississippi":
                stateAbbreviation = "ms";
                break;

            case "Missouri":
                stateAbbreviation = "mo";
                break;

            case "Montana":
                stateAbbreviation = "mt";
                break;

            case "Nebraska":
                stateAbbreviation = "ne";
                break;

            case "Nevada":
                stateAbbreviation = "nv";
                break;

            case "New Hampshire":
                stateAbbreviation = "nh";
                break;

            case "New Jersey":
                stateAbbreviation = "nj";
                break;

            case "New Mexico":
                stateAbbreviation = "nm";
                break;

            case "New York":
                stateAbbreviation = "ny";
                break;

            case "North Carolina":
                stateAbbreviation = "nc";
                break;

            case "North Dakota":
                stateAbbreviation = "nd";
                break;

            case "Ohio":
                stateAbbreviation = "oh";
                break;

            case "Oklahoma":
                stateAbbreviation = "ok";
                break;

            case "Oregon":
                stateAbbreviation = "or";
                break;

            case "Pennsylvania":
                stateAbbreviation = "pa";
                break;

            case "Rhode Island":
                stateAbbreviation = "ri";
                break;

            case "South Carolina":
                stateAbbreviation = "sc";
                break;

            case "South Dakota":
                stateAbbreviation = "sd";
                break;

            case "Tennessee":
                stateAbbreviation = "tn";
                break;

            case "Texas":
                stateAbbreviation = "tx";
                break;

            case "Utah":
                stateAbbreviation = "ut";
                break;

            case "Vermont":
                stateAbbreviation = "vt";
                break;

            case "Virginia":
                stateAbbreviation = "va";
                break;

            case "Washington":
                stateAbbreviation = "wa";
                break;

            case "West Virginia":
                stateAbbreviation = "wv";
                break;

            case "Wisconsin":
                stateAbbreviation = "wi";
                break;

            case "Wyoming":
                stateAbbreviation = "wy";
                break;
        }
        return stateAbbreviation;
    }
}