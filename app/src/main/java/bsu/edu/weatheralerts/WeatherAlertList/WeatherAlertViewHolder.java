package bsu.edu.weatheralerts.WeatherAlertList;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import bsu.edu.weatheralerts.R;
import bsu.edu.weatheralerts.WeatherAlert.WeatherAlert;
import bsu.edu.weatheralerts.WeatherAlert.WeatherAlertActivity;

class WeatherAlertViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private WeatherAlert mCurrentWeatherAlert;
    private TextView mWeatherAlertIdField;
    private Context mCurrentContext;

    public WeatherAlertViewHolder(LayoutInflater layoutInflater, ViewGroup parent, Context currentContext){
        super(layoutInflater.inflate(R.layout.list_item_weather_alert, parent, false));

        mWeatherAlertIdField = itemView.findViewById(R.id.weatherAlertIdField);
        mCurrentContext = currentContext;

        itemView.setOnClickListener(this);
    }

    public void bind(WeatherAlert weatherAlert){
        mCurrentWeatherAlert = weatherAlert;
        mWeatherAlertIdField.setText(weatherAlert.capEvent);
    }

    @Override
    public void onClick(View view){
        Intent intent = WeatherAlertActivity.newIntent(mCurrentContext, mCurrentWeatherAlert.mWeatherAlertId);
        mCurrentContext.startActivity(intent);
    }

}





