package bsu.edu.weatheralerts.WeatherAlertList;

import android.support.v4.app.Fragment;

import bsu.edu.weatheralerts.SingleFragmentActivity;

public class WeatherAlertListActivity extends SingleFragmentActivity {

    protected Fragment createFragment(){
        return new WeatherAlertListFragment();
    }
}
