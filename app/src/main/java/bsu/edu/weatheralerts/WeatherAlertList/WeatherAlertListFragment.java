package bsu.edu.weatheralerts.WeatherAlertList;


import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Spinner;

import java.util.List;

import bsu.edu.weatheralerts.CAPAlertData.CAPAlertConnection;
import bsu.edu.weatheralerts.R;
import bsu.edu.weatheralerts.WeatherAlert.WeatherAlert;
import bsu.edu.weatheralerts.WeatherAlertStore;

public class WeatherAlertListFragment extends Fragment {
    private RecyclerView mWeatherAlertRecyclerView;
    private View mInflatedFragmentView;
    private WeatherAlertStore mWeatherAlertStore;
    private List<WeatherAlert> mCurrentWeatherAlertsList;
    private Spinner mStateField;

    private String mTargetState;


    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup container, Bundle savedInstanceState){
        mInflatedFragmentView = layoutInflater.inflate(R.layout.fragment_weather_alert_list, container, false);

        setWidgetVariables();
        configureWidgets();

        return mInflatedFragmentView;
    }




    private void setWidgetVariables() {
        mWeatherAlertRecyclerView = mInflatedFragmentView.findViewById(R.id.weatherAlert_recycler_view);
        mStateField = mInflatedFragmentView.findViewById(R.id.spinner_state);
    }




    private void configureWidgets(){
        mWeatherAlertRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        mStateField.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mTargetState = String.valueOf(mStateField.getSelectedItem());
                if(mTargetState.equals("Select a state to see alerts"))
                {
                    return;
                }
                getWeatherAlertsFromRSS();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }





    public void getWeatherAlertsFromRSS() {
        @SuppressLint("StaticFieldLeak") final CAPAlertConnection capAlertConnection = new CAPAlertConnection(mTargetState, getContext()) {

            protected void onPostExecute(WeatherAlertStore currentWeatherAlerts){
                mWeatherAlertStore = WeatherAlertStore.getWeatherAlertStore();
                mWeatherAlertStore = currentWeatherAlerts;
                updateUI();
            }
        };
        capAlertConnection.execute();
    }




    private void updateUI() {
        mCurrentWeatherAlertsList = mWeatherAlertStore.getAllWeatherAlerts();
        WeatherAlertRecyclerViewAdapter weatherAlertRecyclerViewAdapter = new WeatherAlertRecyclerViewAdapter();
        mWeatherAlertRecyclerView.setAdapter(weatherAlertRecyclerViewAdapter);
    }




    private class WeatherAlertRecyclerViewAdapter extends RecyclerView.Adapter<WeatherAlertViewHolder>{
        private Context mCurrentContext = getContext();


        public WeatherAlertRecyclerViewAdapter(){
        }


        @NonNull
        @Override
        public WeatherAlertViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
            return new WeatherAlertViewHolder(layoutInflater, parent, mCurrentContext);
        }


        @Override
        public void onBindViewHolder(@NonNull WeatherAlertViewHolder holder, int position) {
            WeatherAlert weatherAlert = mCurrentWeatherAlertsList.get(position);
            holder.bind(weatherAlert);
        }

        @Override
        public int getItemCount() {
            return mCurrentWeatherAlertsList.size();
        }
    }
}
