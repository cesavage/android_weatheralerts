package bsu.edu.weatheralerts.WeatherAlert;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;

import java.util.UUID;

import bsu.edu.weatheralerts.SingleFragmentActivity;

public class WeatherAlertActivity extends SingleFragmentActivity {

    public static final String EXTRA_WEATHER_ALERT_ID = "bsu.edu.weatheralerts.WeatherAlert.weather_alert_id";

    public static Intent newIntent(Context packageContext, UUID weatherAlertId){
        Intent intent = new Intent(packageContext, WeatherAlertActivity.class);
        intent.putExtra(EXTRA_WEATHER_ALERT_ID, weatherAlertId);
        return intent;
    }

    protected Fragment createFragment(){
        UUID weatherAlertId = (UUID) getIntent().getSerializableExtra(EXTRA_WEATHER_ALERT_ID);
        return WeatherAlertFragment.newInstance(weatherAlertId);
    }
}
