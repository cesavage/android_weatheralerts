package bsu.edu.weatheralerts.WeatherAlert;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.net.URL;
import java.util.UUID;

import bsu.edu.weatheralerts.R;
import bsu.edu.weatheralerts.WeatherAlertStore;

import static bsu.edu.weatheralerts.WeatherAlert.WeatherAlertActivity.EXTRA_WEATHER_ALERT_ID;

public class WeatherAlertFragment extends Fragment {
    private WeatherAlert mWeatherAlert;
    private TextView mWidget_capEvent;
    private TextView mWidget_weatherAlertSummary;
    private TextView mWidget_capEffective;
    private TextView mWidget_capExpires;
    private TextView mWidget_capUrgency;
    private TextView mWidget_capSeverity;
    private TextView mWidget_capCertainty;
    private Button mWidget_buttonLink;

    private static final String ARG_WEATHER_ALERT_ID = "weatherAlertId";

    public static WeatherAlertFragment newInstance(UUID weatherAlertId){
        Bundle fragmentArguments = new Bundle();
        fragmentArguments.putSerializable(ARG_WEATHER_ALERT_ID, weatherAlertId);

        WeatherAlertFragment weatherAlertFragment = new WeatherAlertFragment();
        weatherAlertFragment.setArguments(fragmentArguments);
        return weatherAlertFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        UUID weatherAlertId = (UUID) getArguments().getSerializable(ARG_WEATHER_ALERT_ID);
        mWeatherAlert = WeatherAlertStore.getWeatherAlertStore().getWeatherAlertById(weatherAlertId);
    }

    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup fragmentContainer, Bundle savedInstanceState){
        View inflatedFragmentView = layoutInflater.inflate(R.layout.fragment_weather_alert, fragmentContainer, false);
        setWidgetVariables(inflatedFragmentView);
        configureWidgets();

        return inflatedFragmentView;
    }

    private void setWidgetVariables(View fragmentView){
        mWidget_capEvent = fragmentView.findViewById(R.id.widget_capEvent);
        mWidget_weatherAlertSummary = fragmentView.findViewById(R.id.widget_summary);
        mWidget_capEffective = fragmentView.findViewById(R.id.widget_capEffective);
        mWidget_capExpires = fragmentView.findViewById(R.id.widget_capExpires);
        mWidget_capUrgency = fragmentView.findViewById(R.id.widget_capUrgency);
        mWidget_capSeverity = fragmentView.findViewById(R.id.widget_capSeverity);
        mWidget_capCertainty = fragmentView.findViewById(R.id.widget_capCertainty);
        mWidget_buttonLink = fragmentView.findViewById(R.id.button_link);
    }

    private void configureWidgets(){
        mWidget_capEvent.setText(mWeatherAlert.capEvent);
        mWidget_weatherAlertSummary.setText(mWeatherAlert.summary);
        mWidget_capEffective.setText(mWeatherAlert.capEffective);
        mWidget_capExpires.setText(mWeatherAlert.capExpires);
        mWidget_capUrgency.setText(mWeatherAlert.capUrgency);
        mWidget_capSeverity.setText(mWeatherAlert.capSeverity);
        mWidget_capCertainty.setText(mWeatherAlert.capCertainty);
        mWidget_buttonLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = Uri.parse(mWeatherAlert.link);
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        });
    }
}