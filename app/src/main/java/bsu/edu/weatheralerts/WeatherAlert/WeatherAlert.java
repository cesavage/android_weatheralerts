package bsu.edu.weatheralerts.WeatherAlert;

import java.util.UUID;

public class WeatherAlert {
    public UUID mWeatherAlertId;
    public String capEvent = "Default event";
    public String summary = "Default summary";
    public String capEffective = "Default effective";
    public String capExpires = "Default expiration";
    public String capUrgency = "Default urgency";
    public String capSeverity = "Default severity";
    public String capCertainty = "Default certainty";
    public String link = "Default link";


    public WeatherAlert(){
        mWeatherAlertId = UUID.randomUUID();
    }
}